<?php

namespace Drupal\commerce_gmo_linktypeplus;

/**
 * Remote Id variables.
 */
class RemoteIdConstants {
  const REMOTE_IDS = ['TranID', 'PayPayTrackingID', 'RakutenChargeID',
    'DocomoSettlementCode', 'SbTrackingId', 'AuPayInfoNo', 'UriageNO', 'EposTradeId',
  ];

}
